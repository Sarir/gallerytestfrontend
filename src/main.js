import Vue from 'vue'
import App from './App'

import './components';
import routes from './routes';
import store from './store';

import 'element-ui/lib/theme-chalk/index.css';
import 'es6-promise/auto';

import ElementUI from 'element-ui'

window.axios = require('axios');

Vue.use(ElementUI);

Vue.config.productionTip = false;

new Vue({
    el: '#app',
    router: routes,
    store,
    render: h => h(App)
});
