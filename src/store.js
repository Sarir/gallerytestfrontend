import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const store = new Vuex.Store({
    state: {
        albums: [],
        loading: true
    },

    mutations: {
        setAlbums(state, albums) {
            state.albums = albums;
        },

        addAlbum(state, album) {
            state.albums.push(album);
        },
    },

    getters: {
        getAlbumById: (state) => (id) => {
            return state.albums.find(album => album.id.toString() === id);
        }
    },

    actions: {
        loadAlbums({commit}) {
            return new Promise((resolve, reject) => {
                window.axios.get('/api/albums').then((data) => {
                    commit('setAlbums', data.data.data);
                    resolve();
                })
            });
        },

        addAlbum({commit}, album) {
            return new Promise((resolve, reject) => {
                axios.post('/api/albums', album).then(data => {
                    commit('addAlbum', data.data.data);
                    resolve();
                });
            });
        },

        postPhoto({dispatch}, photo) {
            return new Promise((resolve, reject) => {
                axios.post('/api/photos', photo).then(data => {
                    dispatch('loadAlbums').then(() => resolve());
                });
            });
        },
    }
});

export default store;