import Vue from 'vue';

import HeaderComponent from './HeaderComponent.vue';
import FooterComponent from './FooterComponent.vue';
import AddAlbumComponent from './AddAlbumComponent';
import AlbumsComponent from './AlbumsComponent';
import CardComponent from './CardComponent';
import PhotoFormComponent from './PhotoFormComponent';

Vue.component('header-component', HeaderComponent);
Vue.component('footer-component', FooterComponent);
Vue.component('add-album-component', AddAlbumComponent);
Vue.component('albums-component', AlbumsComponent);
Vue.component('card-component', CardComponent);
Vue.component('photo-form-component', PhotoFormComponent);